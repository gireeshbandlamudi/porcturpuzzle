import React, {Component} from 'react';
import { 
  StyleSheet, 
  Text, 
  View,
  TouchableOpacity,
  SafeAreaView,
  TextInput,
  FlatList,
} from 'react-native';

export default class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      searchText: "",
      count: 50,
      yPosition: 0,
      refreshing: false,
      filteredData: [],
      inverseList: [],
      listHeight: 0,
    };
  }

  // Code for a function which return a promise with list or random data
  task = (from, to) => {
    return new Promise((resolve, reject) => {
      var temp = [];
      for(let i=to; i>=from; i--){
        let r = Math.random().toString(36).substring(7);
        temp.push({
          id: i,
          name: r
        });
      }
      resolve(temp);
    })
  }

  componentDidMount(){
    this.task(0, this.state.count)
    .then((response) => {
      console.log(JSON.stringify(response));
      this.setState({
        inverseList: response,
        filteredData: response,
      }, ()=> {
        setTimeout(() => {
          this.flatList1.scrollToEnd({animated: true});
        }, 500);
      })
    })
  }

  // Code for refresh control.
  _onRefresh = () => {
    this.setState({
      refreshing: true,
    },()=> {
      setTimeout(() => {
        this.task((this.state.count + 1), (this.state.count + 50))
        .then((response) => {
          this.setState({
            refreshing: false,
            inverseList: [...response, ...this.state.inverseList],
            count: (this.state.count + 50),
          },()=> {
            this.searchCustom(this.state.searchText);
            setTimeout(() => {
              this.flatList1.scrollToIndex({index: 50})
            }, 500);
          })
        })
      }, 800);
    });
    
  }

  searchCustom = (text) => {
    this.setState({
      searchText: text,
    },()=> {
      var temp = this.state.inverseList;
      var x = temp.filter((item) => {
        return (item.name.indexOf(text) !== -1)
      });
      this.setState({
        filteredData: x,
      })
    })
  }

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
      <View style={styles.container}>

        {/* Code for button */}
        <TouchableOpacity 
        style={{width: 120, height: 30, justifyContent: 'center', alignItems: 'center', borderWidth: 1, borderColor: '#000000', marginTop: 5, marginBottom: 10, }}
        onPress={()=> this.flatList1.scrollToEnd({animated: true})}
        >
          <Text style={{ fontSize: 20, }}>Go Down</Text>
        </TouchableOpacity>

        {/* Code for search bar */}
        <View style={{width: '100%', height: 80, backgroundColor: 'rgb(213, 213, 213)', justifyContent: 'center', alignItems: 'center', }}>
          <View style={{width: '90%', height: 40, borderColor: 'rgb(150, 150, 150)', borderWidth: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#ffffff', }}>
            <TextInput
              placeholder="Search"
              placeholderTextColor="#000000"
              style={{width: '100%', height: 40, padding: 5, }}
              onChangeText={(text) => this.searchCustom(text)}
              value={this.state.searchText}
              autoCapitalize="none"
            />
          </View>
        </View>

        {/* Code for ScrollView */}
        <View style={{flex: 1, width: '100%',}}>
          <FlatList
            ref={ref=> this.flatList1 =ref}
            showsVerticalScrollIndicator={false}
            data={this.state.filteredData}
            keyExtractor={item=>item.id.toString()}
            renderItem={({item}) => (
              <View key={item.id} style={{width: '100%', height: 60, borderBottomColor: '#dddddd', borderBottomWidth: 1, flexDirection: 'row', alignItems: 'center', }}>
                <Text style={{marginLeft: 10, fontSize: 20, }}>{item.id}</Text>
                <Text style={{marginLeft: 15, fontSize: 20, }}>{item.name}</Text>
              </View>
            )}
            refreshing={this.state.refreshing}
            onRefresh={()=> this._onRefresh()}
          />
        </View>

      </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#ffffff',
  },
});
